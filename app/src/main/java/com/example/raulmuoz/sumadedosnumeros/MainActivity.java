package com.example.raulmuoz.sumadedosnumeros;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    EditText numero1 = (EditText) findViewById(R.id.editText3);
    EditText numero2 = (EditText) findViewById(R.id.editText4);
    TextView lblResultado = (TextView) findViewById(R.id.textView3);

    public void Sumar(View v) {
            int num1 = Integer.parseInt((numero1.getText().toString()));
            int num2 = Integer.parseInt((numero2.getText().toString()));
            int suma = num1 + num2;

            lblResultado.setText("Resultado: " + suma);
            //Para que al hacer click en el boton sumar el teclado desaparezca
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(lblResultado.getWindowToken(), 0);
    }

    public void Limpiar(View v) {
        EditText numero1 = (EditText) findViewById(R.id.editText3);
        EditText numero2 = (EditText) findViewById(R.id.editText4);
        numero1.setText("");
        numero2.setText("");
        lblResultado.setText("");
        numero1.hasFocus();
    }
}
